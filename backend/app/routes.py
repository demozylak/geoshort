from app import app
from flask import request
from .database import DataBase


@app.route('/rest/save', methods=["POST"])
def save_page():
  db = DataBase()
  return db.saveLocs(request.json)
		

@app.route('/rest/load/<data_id>')
def extract_page(data_id):
  db = DataBase()
  return db.getById(data_id)
		