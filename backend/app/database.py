from pymongo import MongoClient
import random
from bson.json_util import dumps
import os


cwd = os.path.dirname(os.path.abspath(__file__))


def _generateShortId():
  names = open(
    os.path.join(cwd, '..', 'data', 'imiona.txt')
  ).read().splitlines()

  adjectives = open(
    os.path.join(cwd, '..', 'data', 'przymiotniki.txt')
  ).read().splitlines()

  name = random.choice(names)
  adjective = random.choice(adjectives)
  return adjective.title() + name


class DataBase():
  def __init__(self):
    client = MongoClient('localhost', 27017)
    db = client.geoloc_database
    self.loclists = db.loclists

  def _getUniqueId(self):
    while True:
      short_id = _generateShortId()
      if(self.loclists.count_documents({"shortId": short_id}) == 0):
        return short_id

  def saveLocs(self, loc_array):
    short_id = self._getUniqueId()
    payload = { "shortId": short_id, "data": loc_array}
    self.loclists.insert_one(payload)

    return (dumps({'id': short_id}))

  def getById(self, data_id):
    page = self.loclists.find_one({"shortId": data_id})
    if(page):
      return dumps({"result": "OK", "data": page})
    else:
      return dumps({"result": "ERROR", "reason": "not found"})