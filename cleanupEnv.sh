#!/bin/sh

echo "Removing dependencies, killing containers"

cd frontend && rm -rf node_modules

docker kill $(docker ps -q)
