import React, { createRef, Component } from 'react'
import styled from 'styled-components';
import MapComponent from './map/Map';
import Button from './Button'
import apiClient from '../utils/apiClient';
import { Link } from "react-router-dom";
import uuidv1 from 'uuid/v1';
import Logo from './Logo'

const MapScreenContainer = styled.div`
  display: grid;
  grid-template-columns: auto;
  grid-template-rows: auto 1fr auto;
  justify-items: center;
  width: 100%;
`;

const MapInputPanel = styled.div`
  grid-row: 1;
`;

const MapContainer = styled.div`
  grid-row: 2;
  background-color: white;
  width: 60%;
  border-radius: 15px;
`;

const MapControls = styled.div`
  grid-row: 3;
  display: grid;
  grid-template-columns: auto 1fr auto;
  grid-template-rows: auto;
  width: 40%;
  padding: 2%;
`;

const ClearButton = styled.div`
  grid-column: 1;
`;

const SubmitButton = styled.div`
  grid-column: 3;
`;

export default class MapScreen extends Component {
  state = {
    createdId: "",
    markers: [
      {
        uuid: uuidv1(),
        lat: 51.505,
        lng: -0.09
      }
    ]
  };

  onMarkerAddition = newMarker => {
    this.setState({
      markers: [ newMarker, ...this.state.markers ]
    });
  }

  onMarkerRemoval = markerUuid => {
    this.setState({
      markers: this.state.markers.filter(marker => marker.uuid !== markerUuid)
    });
  }

  handleSubmit = async () => {
    const onError = e => alert(`Failed to create. Error: ${e}`);

    let response = {};
    try {
      response = await apiClient.postCreate({markers: this.state.markers})
    } catch (e) {
      onError(e);
    }
    
    const { status } = response;
    if (status !== 200) {
      onError(`Expected 200 got ${status}`);
    }

    this.setState({
      createdId: response.data.id
    })
  };

  clearMarkers = () => {
    this.setState({markers: []})
  }

  render() {
    if (this.state.createdId !== "") {
      return (
        <MapScreenContainer>
          <MapInputPanel>
            <Logo/>
          </MapInputPanel>
          <MapContainer>
            <MapComponent 
              onMarkerAddition={this.onMarkerAddition}
              onMarkerRemoval={this.onMarkerRemoval}
              markers={this.state.markers}
            />
          </MapContainer>
          <h2>Your link is created!</h2>
          <Link to={`/v/${this.state.createdId}`}>{window.location.href}{`v/${this.state.createdId}`}</Link>
        </MapScreenContainer>
      )
    } else {
      return (
        <MapScreenContainer>
          <MapInputPanel>
            <Logo/>
          </MapInputPanel>
          <MapContainer>
            <MapComponent 
              onMarkerAddition={this.onMarkerAddition}
              onMarkerRemoval={this.onMarkerRemoval}
              markers={this.state.markers}
            />
          </MapContainer>
          <MapControls>
            <ClearButton>
              <Button
                bgColor="red"
                handleClick={this.clearMarkers}
              >
                Clear
              </Button>
            </ClearButton>
            <SubmitButton>
              <Button
                bgColor="green"
                handleClick={this.handleSubmit}
              >
                Submit
              </Button>
            </SubmitButton>
          </MapControls>
        </MapScreenContainer>
      )
    }
  }
}