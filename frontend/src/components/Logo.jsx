import React, { Component } from 'react'
import { Link } from "react-router-dom";

const Logo = () => {
  return (
    <Link to="/" className="logo_link">
      <h1>G E O S H O R T </h1>
    </Link>
  ) 
}

export default Logo;