import React, { Component } from 'react'
import styled from 'styled-components'
import MapComponent from './map/Map';
import apiClient from '../utils/apiClient'
import Logo from './Logo'

const ScreenContainer = styled.div`
  display: grid;
  grid-template-columns: auto;
  grid-template-rows: auto 1fr auto;
  justify-items: center;
  width: 100%;
`;

const MapInputPanel = styled.div`
  grid-row: 1;
`;

const MapContainer = styled.div`
  grid-row: 2;
  background-color: white;
  width: 60%;
  border-radius: 15px;
`;

export default class ViewScreen extends Component {
  state = {
    markers: []
  };

  async componentDidMount() {
    const response = await apiClient.read(this.props.match.params.id);

    const { status } = response;

    if (status !== 200) {
      return alert(`Error while reading data. Expected 200 got ${status}`);
    }

    if (response.data.result != "OK") {
      return alert(
        `Error while reading data. Reason: ${response.data.reason}`
      );
    }

    this.setState({
      markers: response.data.data.data.markers
    })
  }

  render() {
    return (
      <ScreenContainer>
        <h1>Displaying: {this.props.match.params.id}</h1>
        <h3>Locations list:</h3>
        <table style={{ padding: '5px', border: '1px solid black' }}>
          <thead>
            <tr>
              <th>#</th>
              <th>Latitude</th>
              <th>Longtitude</th>
            </tr>
          </thead>
          <tbody>
            {this.state.markers.map((item, index) => (
              <tr>
                <td>{index + 1}</td>
                <td>{item.lat}</td>
                <td>{item.lng}</td>
              </tr>
            ))}
          </tbody>
        </table>

        <MapInputPanel>
          <Logo/>
        </MapInputPanel>
        <MapContainer>
          <MapComponent
            markers={this.state.markers}
            onMarkerAddition={() => { }}
            onMarkerRemoval={() => { }}
            constantMarkers={true}
          />
        </MapContainer>
      </ScreenContainer>
    )
  }
}