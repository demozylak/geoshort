import React, { Component } from "react";
import { Map, Marker, Popup, TileLayer } from "react-leaflet";
import L from "leaflet";
import styled from 'styled-components';

const customMarker = new L.icon({
  iconUrl: "https://unpkg.com/leaflet@1.4.0/dist/images/marker-icon.png",
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize: [41, 41]
});

const MarkerComponent = ({ lat, lng, uuid, onMarkerClick, constant=false}) => {
  const MarkerPopupContainer = styled.div`
    display: grid;
    grid-template-columns: auto 1fr;
    grid-template-rows: auto;
  `;

  const MarkerDescription = styled.div`
    grid-column: 1;
  `;

  const MarkerRemoveBtn = styled.button`
    grid-column: 2;
  `;

  return (
    <Marker position={[lat, lng]} icon={customMarker}>
      <Popup>
        <MarkerPopupContainer>
          <MarkerDescription>
            latitude: <b>{lat}</b><br />longitude: <b>{lng}</b>
          </MarkerDescription>
          {!constant &&
            <MarkerRemoveBtn
              onClick={() => onMarkerClick(uuid)}
            >
              Remove marker
            </MarkerRemoveBtn>
          }
        </MarkerPopupContainer>
      </Popup>
    </Marker>
  )
};

export default MarkerComponent;