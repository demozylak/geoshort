import React, { Component } from "react";
import { Map, Marker, Popup, TileLayer } from "react-leaflet";
import uuidv1 from 'uuid/v1';
import MarkerComponent from './Marker'

export default class  MapComponent extends Component {
  handleMarkerAddition = ( { latlng } ) => {
    const newMarker = {
      uuid: uuidv1(),
      ...latlng
    };

    this.props.onMarkerAddition(newMarker);
  };

  render() {
    const position = this.props.markers.length ? [ this.props.markers[0].lat, this.props.markers[0].lng ] : undefined;
    return (
      <div id="map">
        <Map
          ref={(ref) => { this.map = ref; }}
          style={{ width: '100%', height: '400px' , borderRadius: '15px'}}
          center={position}
          zoom={this.map ? undefined : 7}
          onclick={this.handleMarkerAddition}
        >
          <TileLayer
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          />
          {this.props.markers.map( marker => (
            <MarkerComponent
              lat={marker.lat}
              lng={marker.lng}
              uuid={marker.uuid}
              onMarkerClick={this.props.onMarkerRemoval}
              constant={this.props.constantMarkers}
            />
          ) )}
        </Map>
      </div>
    );
  }
}
